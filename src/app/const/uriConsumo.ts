export const URI_SWAPI = {
    get_species: 'https://swapi.co/api/species/',
    get_specie: 'https://swapi.co/api/species?search=',
    url_swapi: 'https://swapi.co/api/'
}