import { People } from './../models/people';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URI_SWAPI } from '../const/uriConsumo';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UtilService } from './util.service';
@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  private getPeopleUrl: string;
  paginador: any = {};
  constructor(private http: HttpClient,
              private util: UtilService) {
    this.getPeopleUrl = `${URI_SWAPI.url_swapi}people/`;
  }

  loadPersonas(pagina: string): Observable<People[]> {
    const URI = pagina === undefined || pagina === null ? this.getPeopleUrl : `${this.getPeopleUrl}?page=${pagina}`;
    return this.http.get(URI).pipe(
      map((e: any) => {
        this.paginador = { next: this.util.cargarIndice(e.next), previous: this.util.cargarIndice(e.previous),
                           counter: e.count, missing: this.util.paginasrestantes(e.count) };
        return e.results.map(ei => ei);
      })
    );
  }


  loadPersona(nombre: string): Observable<People> {
    return this.http.get(`${URI_SWAPI.url_swapi}people?search=${nombre}`)
      .pipe(
        map((e: any) => {
          return e.results.map(ei => ei);
        })
      );
  }
}
