import { Films } from './../models/films';
import { URI_SWAPI } from './../const/uriConsumo';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UtilService } from './util.service';
@Injectable({
  providedIn: 'root'
})
export class FilmsService {
  private getFilmUrl: string;
  paginador: any = {};
  constructor(private http: HttpClient,
              private util: UtilService) {
    this.getFilmUrl = `${URI_SWAPI.url_swapi}films/`;
  }

  loadFilmList(pagina: string): Observable<Films[]> {
    const URI = pagina === undefined || pagina === null ? this.getFilmUrl : `${this.getFilmUrl}?page=${pagina}`;
    return this.http.get(URI).pipe(
      map((e: any) => {
        this.paginador = { next: this.util.cargarIndice(e.next), previous: this.util.cargarIndice(e.previous),
                           counter: e.count, missing: this.util.paginasrestantes(e.count) };
        return e.results.map(ei => ei);
      })
    );
  }

  loadPelicula(nombre: string): Observable<Films> {
    return this.http.get(`${URI_SWAPI.url_swapi}films?search=${nombre}`)
      .pipe(
        map((e: any) => {
          return e.results.map((ei: Films) => ei);
        })
      );
  }
}
