import { Films } from './../../models/films';
import { Component, OnInit } from '@angular/core';
import { FilmsService } from 'src/app/services/films.service';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {
  detalle: boolean;
  listaPeliculas: Films[] = [];
  pelicula: Films;
  total: number;
  siguiente: number;
  anterior: number;
  paginasFaltantes: number;
  constructor(public service: FilmsService) { }

  ngOnInit() {
    this.loadPeliculaList();
  }
  inicializarPaginador() {
    this.siguiente = this.service.paginador.next;
    this.anterior = this.service.paginador.previous;
    this.total = this.service.paginador.counter;
    this.paginasFaltantes = this.service.paginador.missing;
  }

  loadPeliculaList(pagina?: string) {
    this.service.loadFilmList(pagina).subscribe(data => { this.listaPeliculas = data; this.inicializarPaginador(); });
  }

  loadPelicula(nombre: string) {
     this.service.loadPelicula(nombre).subscribe((data: Films) => {
      this.pelicula = data[0];
      this.detalle = this.pelicula !== null && this.pelicula !== undefined;
    });
  }

  recibePelicula(films: Films) {
    this.detalle = true;
    this.pelicula = films;
  }
  recibeVolver(detalle: boolean) {
    this.detalle = detalle;
  }
}
