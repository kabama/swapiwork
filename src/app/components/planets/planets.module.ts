import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanetsComponent } from './planets.component';
import { Routes, RouterModule } from '@angular/router';
import { DetailComponent } from './detail/detail.component';
import { ListComponent } from './list/list.component';
import { SearchComponent } from './search/search.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared/shared.module';


const routes: Routes = [
  {
    path: '',
    component: PlanetsComponent
  }
];

@NgModule({
  declarations: [PlanetsComponent, DetailComponent, ListComponent, SearchComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes), ReactiveFormsModule, SharedModule
  ], exports: [RouterModule]
})
export class PlanetsModule { }
