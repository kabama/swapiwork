import { Planets } from './../../models/planets';
import { Component, OnInit } from '@angular/core';
import { PlantesService } from 'src/app/services/plantes.service';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.css']
})
export class PlanetsComponent implements OnInit {

  detalle: boolean;
  listaPlanetas: Planets[] = [];
  planeta: Planets;
  total: number;
  siguiente: number;
  anterior: number;
  paginasFaltantes: number;
  constructor(public service: PlantesService) { }

  ngOnInit() {
    this.loadPlanetaList();
  }
  inicializarPaginador() {
    this.siguiente = this.service.paginador.next;
    this.anterior = this.service.paginador.previous;
    this.total = this.service.paginador.counter;
    this.paginasFaltantes = this.service.paginador.missing;
  }

  loadPlanetaList(pagina?: string) {
    this.service.loadPlanetas(pagina).subscribe(data => { this.listaPlanetas = data; this.inicializarPaginador(); });
  }

  loadPlaneta(nombre: string) {
     this.service.loadPlaneta(nombre).subscribe((data: Planets) => {
      this.planeta = data[0];
      this.detalle = this.planeta !== null && this.planeta !== undefined;
    });
  }

  recibePlaneta(planeta: Planets) {
    this.detalle = true;
    this.planeta = planeta;
  }
  recibeVolver(detalle: boolean) {
    this.detalle = detalle;
  }

}
