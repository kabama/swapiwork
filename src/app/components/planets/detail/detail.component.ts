import { Planets } from './../../../models/planets';
import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  @Input() planeta: Planets;
  @Output() detailEmmiter = new EventEmitter();
  constructor(private router: ActivatedRoute) { }

  ngOnInit() {
  }

  volver() {
    this.detailEmmiter.emit(false);
  }
}
