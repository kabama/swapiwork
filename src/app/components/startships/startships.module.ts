import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartshipsComponent } from './startships.component';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { ListComponent } from './list/list.component';
import { DetalleComponent } from './detalle/detalle.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: StartshipsComponent
  }
];

@NgModule({
  declarations: [StartshipsComponent, SearchComponent, ListComponent, DetalleComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes), ReactiveFormsModule, SharedModule
  ], exports: [RouterModule]
})
export class StartshipsModule { }
