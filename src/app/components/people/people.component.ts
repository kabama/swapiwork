import { People } from './../../models/people';
import { Component, OnInit } from '@angular/core';
import { PeopleService } from 'src/app/services/people.service';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {
  detalle: boolean;
  listaPersonas: People[] = [];
  persona: People;
  total: number;
  siguiente: number;
  anterior: number;
  paginasFaltantes: number;
  constructor(public service: PeopleService) { }

  ngOnInit() {
    this.loadPersonasList();
  }
  inicializarPaginador() {
    this.siguiente = this.service.paginador.next;
    this.anterior = this.service.paginador.previous;
    this.total = this.service.paginador.counter;
    this.paginasFaltantes = this.service.paginador.missing;
  }

  loadPersonasList(pagina?: string) {
    this.service.loadPersonas(pagina).subscribe(data => { this.listaPersonas = data; this.inicializarPaginador(); });
  }

  loadPersona(nombre: string) {
     this.service.loadPersona(nombre).subscribe((data: People) => {
      this.persona = data[0];
      this.detalle = this.persona !== null && this.persona !== undefined;
    });
  }

  recibePersona(persona: People) {
    this.detalle = true;
    this.persona = persona;
  }
  recibeVolver(detalle: boolean) {
    this.detalle = detalle;
  }
}
