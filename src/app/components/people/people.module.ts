import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PeopleComponent } from './people.component';
import { ListComponent } from './list/list.component';
import { SearchComponent } from './search/search.component';
import { DetailComponent } from './detail/detail.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: PeopleComponent
  }
];


@NgModule({
  declarations: [PeopleComponent, ListComponent, SearchComponent, DetailComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes), ReactiveFormsModule,SharedModule
  ],
  exports: [RouterModule]
})
export class PeopleModule { }
